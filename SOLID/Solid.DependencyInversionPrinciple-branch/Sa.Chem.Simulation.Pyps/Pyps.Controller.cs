﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Sa.Chem
{
	public sealed partial class Pyps : Simulation
    {
		public sealed class Controller : AController
		{
			public override string RunFromQueue()
			{
				string factorSetId;
				string refnum;
				string rowsRemaining;

				string cnString = Sa.Database.dbConnectionString("DBS5", "Olefins", "", "");
				string procedure = "[sim].[Select_List]";

				using (SqlConnection cn = new SqlConnection(cnString))
				{
					using (SqlCommand cmd = new SqlCommand(procedure, cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Connection.Open();

						using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
						{
							while (rdr.Read())
							{
								factorSetId = rdr.GetString(rdr.GetOrdinal("FactorSetId"));
								refnum = rdr.GetString(rdr.GetOrdinal("Refnum"));
								rowsRemaining = rdr.GetString(rdr.GetOrdinal("RowsRemaining"));

								Console.WriteLine(rowsRemaining + "\t" + refnum);

								LoopFeeds(factorSetId, refnum);
							}
						}
						cmd.Connection.Close();
					}
				}

				return "ok";
			}

			public override void LoopFeeds(string factorSetId, string refnum)
			{
				string cString = Sa.Database.dbConnectionString("DBS5", "Olefins", "", "");
				string procedure = "[sim].[Select_SimulationInputPyps]";

				using (SqlConnection cn = new SqlConnection(cString))
				{
					using (SqlCommand cmd = new SqlCommand(procedure, cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@FactorSetId", System.Data.SqlDbType.VarChar, 12).Value = factorSetId;
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (Pyps.Engine e = new Pyps.Engine())
						{
							e.LoopFeeds(cmd);
						}
					}
				}
			}

			//	TODO: Implement RunOneFeed for a well formatted text file
			//
			//	The database call has been created, but now we want to run only one feed. This feed
			//	has been provided in a text file (FormattedString.txt). This file ready to read and
			//	will execute successfully. The simulation has been vetted, tested, and cannot be
			//	changed as it will break all tests. But feel free to examine all the code for help.
			//
			//	After execution, open the MDLOUT.DAT file in the Console.
			//
			//	TWO CONSTRAINTS:
			//	1) Only change the method RunOneFeed()
			//	2) Only extend Sa.Chem.Pyps.Controller (this class)
			//
			//	Run the program, click the button "Run one file" (Form1.button1).
			public override string RunOneFeed()
			{
				string path = System.IO.Directory.GetCurrentDirectory() + "\\Development\\FormattedString.txt";

				Console.WriteLine("Running simulation for: " + Pyps.simModelId);
				Console.WriteLine("File Exists: " + System.IO.File.Exists(path));

				string factorSetId = "2013";
				string refnum = "2013PCH999";
				string opCondId = "OSOP";
				string streamId = "Naphtha";
				int recycleId = 0;

				using (Pyps.Engine e = new Pyps.Engine())
				{
					//	Hint: Enter your code here.
					//	This should be the only area where you need to change RunOneFeed.
				}

				return "Success";
			}
		}
	}
}
